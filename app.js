const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const port = 3000

app.use(bodyParser.json())
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
)
//app.get('/', (request, response) => {
  //  response.json({ info: 'Node.js, Express, and Postgres API' })
 // })
app.listen(port, () => {
    console.log(`App running on port ${port}.`)
  })



//Frontend setup done

const Pool = require('pg').Pool
const pool = new Pool({
  user: 'postgres',
  host: 'localhost',
  database: 'myFirstDb',
  password: 'Jan@2022',
  port: 5432,
})



//Connection  to DB done


const getCars = (request, response) => {
    pool.query('SELECT * FROM cars ORDER BY id ASC', (error, results) => {
      if (error) {
        throw error
      }
      response.status(200).json(results.rows)
    })
  }

  app.get ('/cars', getCars)


  const getBuses = (request, response) => {
    pool.query('SELECT * FROM buses ORDER BY id ASC', (error, results) => {
      if (error) {
        throw error
      }
      response.status(200).json(results.rows)
    })
  }

  app.get ('/buses', getBuses)